:set background=dark
:set bs=2
:set expandtab
:set history=1000
:set hlsearch
:set ignorecase
:set incsearch
:set nobackup
:set nocompatible
:set noswapfile
:set nowritebackup
:set number
:set shiftwidth=4
:set showcmd
:set showmatch
:set tabstop=4
:set tw=79
:set wildmenu
:setlocal cm=blowfish2
:syntax on
filetype plugin indent on
